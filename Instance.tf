
# server creation
resource "aws_instance" "server_pub_1" {
  ami                         = var.ami # eu-west-2
  instance_type               = var.instance
  key_name                    = "server_keypair"
  vpc_security_group_ids      = [aws_security_group.Kenny_SG.id]
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_sub_1.id

  tags = {
    name = var.enviroment_name
  }


}


resource "aws_instance" "server_pub_2" {
  ami                         = var.ami # eu-west-2
  instance_type               = var.instance
  key_name                    = "server_keypair"
  vpc_security_group_ids      = [aws_security_group.Kenny_SG.id]
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_sub_2.id

  tags = {
    name = var.enviroment_name
  }


}


resource "aws_instance" "server_pub_3" {
  ami                         = var.ami # eu-west-2
  instance_type               = var.instance
  key_name                    = "server_keypair"
  vpc_security_group_ids      = [aws_security_group.Kenny_SG.id]
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_sub_3.id

  tags = {
    name = var.enviroment_name
  }


}