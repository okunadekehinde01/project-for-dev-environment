
#VPC
resource "aws_vpc" "Project_2_vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "Project_2_vpc"
  }
}


#Public Subnet
resource "aws_subnet" "public_sub_1" {
  vpc_id            = aws_vpc.Project_2_vpc.id
  cidr_block        = var.pub_sub_1
  availability_zone = var.az_1

  tags = {
    Name = "Public_sub_1"
  }
}


#Public Subnet
resource "aws_subnet" "public_sub_2" {
  vpc_id            = aws_vpc.Project_2_vpc.id
  cidr_block        = var.pub_sub_2
  availability_zone = var.az_2

  tags = {
    Name = "Public_sub_2"
  }
}


#Public Subnet
resource "aws_subnet" "public_sub_3" {
  vpc_id            = aws_vpc.Project_2_vpc.id
  cidr_block        = var.pub_sub_3
  availability_zone = var.az_3

  tags = {
    Name = "Public_sub_3"
  }
}


#Private Subnet
resource "aws_subnet" "Private_sub_1" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.priv_sub_1

  tags = {
    Name = "Private_sub_1"
  }
}


#Private Subnet
resource "aws_subnet" "Private_sub_2" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.priv_sub_2

  tags = {
    Name = "Private_sub_2"
  }
}


#Private Subnet
resource "aws_subnet" "Private_sub_3" {
  vpc_id     = aws_vpc.Project_2_vpc.id
  cidr_block = var.priv_sub_3

  tags = {
    Name = "Private_sub_3"
  }
}


# Public route table
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.Project_2_vpc.id

  tags = {
    Name = "public-route-table"
  }
}


# Private route table
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.Project_2_vpc.id

  tags = {
    Name = "private-route-table"
  }
}


# Public route table association
resource "aws_route_table_association" "public-route-1-association" {
  subnet_id      = aws_subnet.public_sub_1.id
  route_table_id = aws_route_table.public-route-table.id
}


# Public route table association
resource "aws_route_table_association" "public-route-2-association" {
  subnet_id      = aws_subnet.public_sub_2.id
  route_table_id = aws_route_table.public-route-table.id
}


# Public route table association
resource "aws_route_table_association" "public-route-3-association" {
  subnet_id      = aws_subnet.public_sub_3.id
  route_table_id = aws_route_table.public-route-table.id
}


# Private route table association
resource "aws_route_table_association" "private-route-1-association" {
  subnet_id      = aws_subnet.Private_sub_1.id
  route_table_id = aws_route_table.private-route-table.id
}


# Private route table association
resource "aws_route_table_association" "private-route-2-association" {
  subnet_id      = aws_subnet.Private_sub_2.id
  route_table_id = aws_route_table.private-route-table.id
}


# Private route table association
resource "aws_route_table_association" "private-route-3-association" {
  subnet_id      = aws_subnet.Private_sub_3.id
  route_table_id = aws_route_table.private-route-table.id
}


# Internet gateway
resource "aws_internet_gateway" "Project_2_IGW" {
  vpc_id = aws_vpc.Project_2_vpc.id

  tags = {
    Name = "Project_2_IGW"
  }
}

# Route
resource "aws_route" "public-IGW-route" {
  route_table_id         = aws_route_table.public-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.Project_2_IGW.id
}

