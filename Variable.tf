# Variables

variable "instance" {
  description = "type of instance"
  default     = "t2.micro"

}

variable "ami" {
  description = "type of ami"
  default     = "ami-030770b178fa9d374"

}

variable "region" {
  description = "type of region"
  default     = "eu-west-2"

}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "pub_sub_1" {
  default = "10.0.1.0/24"
}

variable "pub_sub_2" {
  default = "10.0.2.0/24"
}

variable "pub_sub_3" {
  default = "10.0.3.0/24"
}

variable "priv_sub_1" {
  default = "10.0.4.0/24"
}

variable "priv_sub_2" {
  default = "10.0.5.0/24"
}

variable "priv_sub_3" {
  default = "10.0.6.0/24"
}


variable "az_1" {
  default = "eu-west-2a"
}

variable "az_2" {
  default = "eu-west-2b"
}

variable "az_3" {
  default = "eu-west-2c"
}


variable "enviroment_name" {}

