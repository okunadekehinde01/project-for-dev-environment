#Application load balancer

resource "aws_lb" "Kenny_lb" {
  name               = "application-lb"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [aws_security_group.Kenny_SG.id]
  subnets            = [aws_subnet.public_sub_1.id, aws_subnet.public_sub_2.id, aws_subnet.public_sub_3.id]
  ip_address_type    = "ipv4"
}


#Target group
resource "aws_lb_target_group" "Kenny-lb-tg" {
  name        = "Kenny-lb-tg"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.Project_2_vpc.id

  health_check {
    protocol            = "HTTP"
    path                = "/"
    port                = 80
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
  }
}

#Listener
resource "aws_lb_listener" "ALB-Listener" {
  load_balancer_arn = aws_lb.Kenny_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.Kenny-lb-tg.arn
  }
}



