#Data base creation

resource "aws_db_instance" "Kenny" {
  allocated_storage    = 10
  engine               = "MYSQL"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  username             = "Kenny"
  password             = "cloudrock"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}


#Mysql security group

resource "aws_security_group" "mysql-sg" {
  name        = "mysql-sg"
  description = "Allow mysql inbound traffic"
  vpc_id      = aws_vpc.Project_2_vpc.id

  ingress {
    description = "TLS from mysql"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}